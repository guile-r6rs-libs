;;; run-test.scm --- Run a test file and exit with an appropriate status.
;;;
;;; Jim Blandy <jimb@red-bean.com> --- May 1999
;;;
;;; Copyright (C) 1999, 2001, 2006 Free Software Foundation, Inc.
;;; Copyright (C) 2007 Ludovic Court�s <ludovic.courtes@laas.fr>
;;;
;;; This program is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 2, or (at your option)
;;; any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this software; see the file COPYING.  If not, write to
;;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;;; Boston, MA 02110-1301 USA

;;; Code borrowed from `test-suite/guile-test' in the core Guile
;;; distribution.

(use-modules (test-suite lib))

(let ((tests (cdr (command-line)))
      (global-pass #t)
      (counter (make-count-reporter)))
  (register-reporter (car counter))
  (register-reporter user-reporter)
  (register-reporter (lambda results
                       (case (car results)
                         ((unresolved)
                          #f)
                         ((fail upass error)
                          (set! global-pass #f)))))

  ;; Run the tests.
  (for-each (lambda (test)
              (display (string-append "Running `" test "'\n"))
              (save-module-excursion
                (lambda ()
                  (with-test-prefix test (primitive-load test)))))
            tests)

  ;; Display the final counts, both to the user and in the log
  ;; file.
  (let ((counts ((cadr counter))))
    (print-counts counts))

  (quit global-pass))

;;; arch-tag: f27521e3-3745-4ae0-a625-2d34ce3ffd2e
