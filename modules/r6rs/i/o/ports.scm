;;; Guile-R6RS-Libs --- Implementation of R6RS standard libraries.
;;; Copyright (C) 2007  Ludovic Court�s <ludovic.courtes@laas.fr>
;;;
;;; Guile-R6RS-Libs is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public
;;; License as published by the Free Software Foundation; either
;;; version 2.1 of the License, or (at your option) any later version.
;;;
;;; Guile-R6RS-Libs is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with Guile-R6RS-Libs; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

(define-module (r6rs i/o ports))

;;;
;;; This module provides a compatibility layer with R5.92RS, which was
;;; provided in Guile-R6RS-Libs 0.0.  It should not be used by new programs.
;;;

(let ((iface (resolve-interface '(r6rs io ports))))
  (set-module-public-interface! (current-module) iface))

