;;; Guile-R6RS-Libs --- Implementation of R6RS standard libraries.
;;; Copyright (C) 2009  Ludovic Courtès <ludo@gnu.org>
;;;
;;; Guile-R6RS-Libs is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public
;;; License as published by the Free Software Foundation; either
;;; version 2.1 of the License, or (at your option) any later version.
;;;
;;; Guile-R6RS-Libs is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with Guile-R6RS-Libs; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

(define-module (r6rs io ports))

;;;
;;; This module provides a compatibility layer with Guile-R6RS-Libs 0.1.  It
;;; should not be used by new programs.
;;;

(let ((iface (resolve-interface '(rnrs io ports))))
  (module-for-each (lambda (name var)
                     (module-define! (current-module)
                                     name (variable-ref var))
                     (module-export! (current-module) (list name)))
                   iface))

(define (make-custom-binary-input-port id read! . args)
  ;; Before Guile-R6RS-Libs 0.2, `make-custom-binary-input-port' erroneously
  ;; considered the last 3 arguments as optional.  This version is kept here
  ;; for compatibility.

  (define make-cbip
    ;; The real procedure, which requires 5 arguments.
    (@ (rnrs io ports) make-custom-binary-input-port))

  (let ((args (cond ((null? args)        '(#f #f #f))
                    ((null? (cdr args))  `(#f #f ,@args))
                    ((null? (cddr args)) `(#f ,@args))
                    (else args))))
    (apply make-cbip id read! args)))
