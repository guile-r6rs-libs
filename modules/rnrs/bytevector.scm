;;; Guile-R6RS-Libs --- Implementation of R6RS standard libraries.
;;; Copyright (C) 2007, 2009  Ludovic Court�s <ludo@gnu.org>
;;;
;;; Guile-R6RS-Libs is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public
;;; License as published by the Free Software Foundation; either
;;; version 2.1 of the License, or (at your option) any later version.
;;;
;;; Guile-R6RS-Libs is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with Guile-R6RS-Libs; if not, write to the Free Software
;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

(define-module (rnrs bytevector)
  :export-syntax (endianness)
  :export (native-endianness bytevector?
           make-bytevector bytevector-length bytevector=? bytevector-fill!
           bytevector-copy! bytevector-copy bytevector-u8-ref
           bytevector-s8-ref
           bytevector-u8-set! bytevector-s8-set! bytevector->u8-list
           u8-list->bytevector
           bytevector-uint-ref bytevector-uint-set!
           bytevector-sint-ref bytevector-sint-set!
           bytevector->sint-list bytevector->uint-list
           uint-list->bytevector sint-list->bytevector

           bytevector-u16-ref bytevector-s16-ref
           bytevector-u16-set! bytevector-s16-set!
           bytevector-u16-native-ref bytevector-s16-native-ref
           bytevector-u16-native-set! bytevector-s16-native-set!

           bytevector-u32-ref bytevector-s32-ref
           bytevector-u32-set! bytevector-s32-set!
           bytevector-u32-native-ref bytevector-s32-native-ref
           bytevector-u32-native-set! bytevector-s32-native-set!

           bytevector-u64-ref bytevector-s64-ref
           bytevector-u64-set! bytevector-s64-set!
           bytevector-u64-native-ref bytevector-s64-native-ref
           bytevector-u64-native-set! bytevector-s64-native-set!

           bytevector-ieee-single-ref
           bytevector-ieee-single-set!
           bytevector-ieee-single-native-ref
           bytevector-ieee-single-native-set!

           bytevector-ieee-double-ref
           bytevector-ieee-double-set!
           bytevector-ieee-double-native-ref
           bytevector-ieee-double-native-set!

           string->utf8 string->utf16 string->utf32
           utf8->string utf16->string utf32->string))


(load-extension "libguile-r6rs-libs-v-0" "scm_init_r6rs_bytevector")

(define-macro (endianness sym)
  (if (memq sym '(big little))
      `(quote ,sym)
      (error "unsupported endianness" sym)))

;;; arch-tag: 87dcf8cd-88a6-4489-8370-32ef5b3b1d62
