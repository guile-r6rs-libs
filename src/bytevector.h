/* Guile-R6RS-Libs --- Implementation of R6RS standard libraries.
   Copyright (C) 2007, 2009  Ludovic Court�s <ludo@gnu.org>

   Guile-R6RS-Libs is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   Guile-R6RS-Libs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with Guile-R6RS-Libs; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  */

#ifndef GUILE_R6RS_BYTEVECTOR
#define GUILE_R6RS_BYTEVECTOR

/* R6RS bytevectors.  */

#include <libguile.h>

SCM_API void scm_init_r6rs_bytevector (void);

#define SCM_VALIDATE_R6RS_BYTEVECTOR(_pos, _obj)		\
  SCM_VALIDATE_SMOB ((_pos), (_obj), r6rs_bytevector);

#define SCM_R6RS_BYTEVECTOR_LENGTH(_bv)		\
  ((unsigned) SCM_SMOB_DATA (_bv))
#define SCM_R6RS_BYTEVECTOR_CONTENTS(_bv)		\
  (SCM_R6RS_BYTEVECTOR_INLINE_P (_bv)			\
   ? (signed char *) SCM_SMOB_OBJECT_2_LOC (_bv)	\
   : (signed char *) SCM_SMOB_DATA_2 (_bv))


SCM_API SCM scm_r6rs_endianness_big;
SCM_API SCM scm_r6rs_endianness_little;

SCM_API SCM scm_r6rs_make_bytevector (SCM, SCM);
SCM_API SCM scm_r6rs_c_make_bytevector (unsigned);
SCM_API SCM scm_r6rs_native_endianness (void);
SCM_API SCM scm_r6rs_bytevector_p (SCM);
SCM_API SCM scm_r6rs_bytevector_length (SCM);
SCM_API SCM scm_r6rs_bytevector_eq_p (SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_fill_x (SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_copy_x (SCM, SCM, SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_copy (SCM);

SCM_API SCM scm_r6rs_bytevector_to_u8_list (SCM);
SCM_API SCM scm_r6rs_u8_list_to_bytevector (SCM);
SCM_API SCM scm_r6rs_uint_list_to_bytevector (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_to_uint_list (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_sint_list_to_bytevector (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_to_sint_list (SCM, SCM, SCM);

SCM_API SCM scm_r6rs_bytevector_u16_native_ref (SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_s16_native_ref (SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_u32_native_ref (SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_s32_native_ref (SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_u64_native_ref (SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_s64_native_ref (SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_u8_ref (SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_s8_ref (SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_uint_ref (SCM, SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_sint_ref (SCM, SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_u16_ref (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_s16_ref (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_u32_ref (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_s32_ref (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_u64_ref (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_s64_ref (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_u16_native_set_x (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_s16_native_set_x (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_u32_native_set_x (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_s32_native_set_x (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_u64_native_set_x (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_s64_native_set_x (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_u8_set_x (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_s8_set_x (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_uint_set_x (SCM, SCM, SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_sint_set_x (SCM, SCM, SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_u16_set_x (SCM, SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_s16_set_x (SCM, SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_u32_set_x (SCM, SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_s32_set_x (SCM, SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_u64_set_x (SCM, SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_s64_set_x (SCM, SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_ieee_single_ref (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_ieee_single_native_ref (SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_ieee_single_set_x (SCM, SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_ieee_single_native_set_x (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_ieee_double_ref (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_ieee_double_native_ref (SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_ieee_double_set_x (SCM, SCM, SCM, SCM);
SCM_API SCM scm_r6rs_bytevector_ieee_double_native_set_x (SCM, SCM, SCM);
SCM_API SCM scm_r6rs_string_to_utf8 (SCM);
SCM_API SCM scm_r6rs_string_to_utf16 (SCM, SCM);
SCM_API SCM scm_r6rs_string_to_utf32 (SCM, SCM);
SCM_API SCM scm_r6rs_utf8_to_string (SCM);
SCM_API SCM scm_r6rs_utf16_to_string (SCM, SCM);
SCM_API SCM scm_r6rs_utf32_to_string (SCM, SCM);



/* Internal API.  */

/* The threshold (in octets) under which bytevectors are stored "in-line",
   i.e., without allocating memory beside the SMOB itself (a double cell).
   This optimization is necessary since small bytevectors are expected to be
   common.  */
#define SCM_R6RS_BYTEVECTOR_INLINE_THRESHOLD  (2 * sizeof (SCM))
#define SCM_R6RS_BYTEVECTOR_INLINEABLE_SIZE_P(_size)	\
  ((_size) <= SCM_R6RS_BYTEVECTOR_INLINE_THRESHOLD)
#define SCM_R6RS_BYTEVECTOR_INLINE_P(_bv)				     \
  (SCM_R6RS_BYTEVECTOR_INLINEABLE_SIZE_P (SCM_R6RS_BYTEVECTOR_LENGTH (_bv)))

/* Hint that is passed to `scm_gc_malloc ()' and friends.  */
#define SCM_R6RS_GC_BYTEVECTOR "r6rs-bytevector"

SCM_API scm_t_bits scm_tc16_r6rs_bytevector;
SCM_API SCM scm_r6rs_c_take_bytevector (signed char *, unsigned);

#define scm_r6rs_c_shrink_bytevector(_bv, _len)		\
  (SCM_R6RS_BYTEVECTOR_INLINE_P (_bv)			\
   ? (_bv)						\
   : scm_r6rs_i_shrink_bytevector ((_bv), (_len)))

SCM_API SCM scm_r6rs_i_shrink_bytevector (SCM, unsigned);
SCM_API SCM scm_r6rs_null_bytevector;

#endif

/* arch-tag: b727a1e1-ed81-4c92-8152-538928090c84
 */
