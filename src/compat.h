/* Guile-R6RS-Libs --- Implementation of R6RS standard libraries.
   Copyright (C) 2008, 2009  Ludovic Courtès <ludo@gnu.org>

   Guile-R6RS-Libs is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   Guile-R6RS-Libs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with Guile-R6RS-Libs; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  */

#ifndef GUILE_R6RS_COMPAT_H
#define GUILE_R6RS_COMPAT_H

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* The `scm_set_port_read ()' method was once proposed to provide a flexible
   and better performing replacement for the `fill_input' port method:
   http://lists.gnu.org/archive/html/guile-devel/2008-06/msg00004.html .
   It has not made it (yet?) into a Guile release.  */

#ifdef HAVE_SCM_SET_PORT_READ
# define PORT_FILL_INPUT_METHOD(m)  NULL
#else
# define PORT_FILL_INPUT_METHOD(m)  (m)
# define scm_set_port_read(p, r)    do { } while (0)
#endif

#endif
