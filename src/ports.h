/* Guile-R6RS-Libs --- Implementation of R6RS standard libraries.
   Copyright (C) 2007, 2009  Ludovic Court�s <ludo@gnu.org>

   Guile-R6RS-Libs is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   Guile-R6RS-Libs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with Guile-R6RS-Libs; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  */

#ifndef GUILE_R6RS_IO_PORTS_H
#define GUILE_R6RS_IO_PORTS_H

/* R6RS I/O Ports.  */

#include <libguile.h>

SCM_API void scm_init_r6rs_ports (void);

SCM_API SCM scm_r6rs_eof_object (void);
SCM_API SCM scm_r6rs_open_bytevector_input_port (SCM, SCM);
SCM_API SCM scm_r6rs_make_custom_binary_input_port (SCM, SCM, SCM, SCM, SCM);
SCM_API SCM scm_r6rs_get_u8 (SCM);
SCM_API SCM scm_r6rs_lookahead_u8 (SCM);
SCM_API SCM scm_r6rs_get_bytevector_n (SCM, SCM);
SCM_API SCM scm_r6rs_get_bytevector_n_x (SCM, SCM, SCM, SCM);
SCM_API SCM scm_r6rs_get_bytevector_some (SCM);
SCM_API SCM scm_r6rs_get_bytevector_all (SCM);
SCM_API SCM scm_r6rs_put_u8 (SCM, SCM);
SCM_API SCM scm_r6rs_put_bytevector (SCM, SCM, SCM, SCM);
SCM_API SCM scm_r6rs_open_bytevector_output_port (SCM);
SCM_API SCM scm_r6rs_make_custom_binary_output_port (SCM, SCM, SCM, SCM, SCM);

#endif
