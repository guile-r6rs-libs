/* Guile-R6RS-Libs --- Implementation of R6RS standard libraries.
   Copyright (C) 2007, 2009  Ludovic Court�s <ludo@gnu.org>

   Guile-R6RS-Libs is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   Guile-R6RS-Libs is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with Guile-R6RS-Libs; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  */

#ifndef GUILE_R6RS_UTILS_H
#define GUILE_R6RS_UTILS_H

/* Common utilities.  */

/* `SCM_LIKELY ()' and `SCM_UNLIKELY ()' appeared sometime during the Guile
   1.8 release series.  */

#ifndef SCM_LIKELY
# ifdef __GNUC__
#  define SCM_LIKELY(_cond)    (__builtin_expect (!!(_cond), 1))
#  define SCM_UNLIKELY(_cond)  (__builtin_expect ((_cond), 0))
# else
#  define SCM_LIKELY(_cond)    (_cond)
#  define SCM_UNLIKELY(_cond)  (_cond)
# endif
#endif

#endif

/* arch-tag: 878d0655-2504-4154-bf27-fab06652ebd5
 */
